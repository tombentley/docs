

# Strimzi Quickstart

Getting up and running with a Apache Kafka cluster on Kubernetes and OKD can be very simple, when using the Strimzi project! This quick start guide walks you through a quick and easy install for your development environment, using OKD.


## Start OKD 

This assumes that you have the latest version of the `oc` binary, which you can get [here](https://github.com/OKD/origin/releases).

```shell
oc cluster up
```

This will start a local installation of OKD. Once this is completed, login as a `cluster-admin` user:


```shell
# Install as cluster-admin
oc login -u system:admin

# Setup Strimzi
oc project myproject
```

Next we are applying the Strimzi install files, including `ClusterRoles`, `ClusterRoleBindings` and some **Custom Resource Definitions** (`CRDs`) for the Apache Kafka itself, or creating Apache Kafka topics:

```shell
oc apply -f https://github.com/strimzi/strimzi-kafka-operator/releases/download/0.8.1/strimzi-cluster-operator-0.8.1.yaml -n myproject
```

Next, we feed Strimzi with a simple **Custom Resource**, which will than give you a simple, ephemeral Apache Kafka Cluster:

```shell
# Apply the `Kafka` Cluster CR file, with exposed IP
oc apply -f https://gist.githubusercontent.com/matzew/a57485deff91591a442dcf6c7ab16c87/raw/4c90042c8b7151b614b8a81ccb1a76955046348c/strimzi.yaml -n myproject
```

We can now watch the deployment, and see all required pods being created:

```shell
watch oc get pods -n myproject
```

Now, we need to lookup the `EXTERNAL-IP` which will be used connecting your application to Apache Kafka:

```shell
oc get service -n myproject -l strimzi.io/name=my-cluster-kafka-external-bootstrap
```

Enjoy your Apache Kafka cluster, running on OKD!